///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 01b - Summation
//
// Usage:  summation n
//   n:  Sum the digits from 1 to n
//
// Result:
//   The sum of the digits from 1 to n
//
// Example:
//   $ summation 6
//   The sum of the digits from 1 to 6 is 21
//
// @author Jayson Iwanaka <jiwanaka@hawaii.edu>
// @date   12 Jan 2021
///////////////////////////////////////////////////////////////////////////////

/*#include <stdio.h>
#include <stdlib.h>

int main() {
   int n;
   int sum = 0;

   printf("Enter your number:\n");
   scanf("%d", &n);

   for(int x=0; x<=n; x++){
      sum += x;
   }
   printf("The answer is: %d\n", sum);
   return 0;
}*/

//I did not understand how the method worked at first so I created an alternate program, so ignore the above

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[]) {
   int n = atoi(argv[1]);
   int sum = 0;

   for(int x=0; x<=n; x++){
      sum += x;
   }
   printf("The sum of the digits from 1 to %d is %d\n", n, sum);
   return 0;
}
